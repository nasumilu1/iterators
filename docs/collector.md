# How to create custom a Collector?

A collector provides a mutable reduction operation, sound familiar? It is a PHP flavor of the Java Stream API Collector
class. 

## Examples

First a simple class which represents a tangible item of value which may or may not be taxable. Its members are public
ready only for brevity. 

```php
final readonly class TangibleItem implements Stringable
{

    public function __construct(public string      $name,
                                public float       $value,
                                public float|null  $tax = null)
    {}
    
    public function taxable(): bool
    {
        return $this->tax != null;
    }

    public function total(): float
    {
        return $this->tax ? $this->value * (1 + $this->tax) : $this->value;
    }

    public function __toString(): string
    {
        return $this->name;
    }
    
    public static function compare(TangibleItem $a, TangibleItem $b): int 
    { 
        return $a->total() <=> $b->total(); 
    }

}
```

Next create a `Generator` which makes a list of items.

```php
function build_items(): Generator
{
    yield new TangibleItem('Flour', 9.99);
    yield new TangibleItem('Candy Bar', 2.95, 0.075);
    yield new TangibleItem('Soda 2L', 1.99, 0.8);
    yield new TangibleItem('Milk', 4.99);
}
```

First, let us get the maximum taxable value by first filtering the iterator so only taxable items are collected. 

```php
$maxValue = Iterators::from(build_items())
    ->filter(static fn(TangibleItem $item): bool => $item->taxable())
    ->collect(Collectors::max(TangibleItem::compare(...)));

echo sprintf(
    "The maximum taxable item is %s which cost $%.2f and taxed $%.2f totaling $%.2f!", 
    $maxValue, 
    $maxValue->value, 
    $maxValue->tax, 
    $maxValue->total()
);
// Expected Output: The maximum taxable item is Candy Bar which cost $2.95 and taxed $0.07 totaling $3.17!
```

We can just as easily get the minimum taxable item using the `Collectors::min`

```php
$minValue = Iterators::from(build_items())
    ->filter(static fn(TangibleItem $item): bool => $item->taxable())
    ->collect(Collectors::min(TangibleItem::compare(...)));

echo sprintf(
    "The minimum taxable item is %s which cost $%.2f and taxed $%.2f totaling $%.2f!",
    $minValue,
    $minValue->value,
    $minValue->tax,
    $minValue->total()
);

// Expected Output: The minimum taxable item is Soda 2L which cost $1.99 and taxed $0.08 totaling $2.15!
```

Finding the number of items, sum and average totals can also be collected.

```php
$totalCostFn = static fn(TangibleItem $item): float => $item->total();

$sum = Iterators::from(build_items())
    ->map($totalCostFn(...))
    ->collect(Collectors::sum());

$count = Iterators::from(build_items())
    ->map($totalCostFn(...))
    ->collect(Collectors::count());

$average = $totalCost = Iterators::from(build_items())
    ->map($totalCostFn(...))
    ->collect(Collectors::average());

echo sprintf("There are %d items which total $%.2f averaging $%.2f!", $count, $sum, $average);

// Expected Output: There are 4 items which total $20.30 averaging $5.08!
```

## Custom Collector

Boy that is a lot of code to just get the count, sum, and average. What if a count, sum, and average for only taxable 
items and non-taxable items? Well that is a good use for creating a `Collector` specific to the task.

A `Collector` is merely a control structure which utilizes as `Supplier` to maintain state between calling an 
accumulator on each iteration and finally an optional finalizer function.

First, lets create the `Supplier` class.

```php
$collector = new class() implements Collector {
            private array $summary = ['taxable' => ['count' => 0,
                'subtotal' => 0,
                'total' => 0,
                'average_before_tax' => 0,
                'average' => 0,],
                'non_taxable' => ['count' => 0,
                    'total' => 0,
                    'average' => 0,]];

            public function accumulate(mixed $value, mixed $key): void
            {
                if ($value->taxable()) {
                    $this->summary['taxable']['count'] += 1;
                    $this->summary['taxable']['subtotal'] += $value->value;
                    $this->summary['taxable']['total'] += $value->total();
                } else {
                    $this->summary['non_taxable']['count'] += 1;
                    $this->summary['non_taxable']['total'] += $value->total();
                }
            }

            public function finisher(): array
            {
                $this->summary['taxable']['average_before_tax'] = $this->summary['taxable']['subtotal'] / $this->summary['taxable']['count'];
                $this->summary['taxable']['average'] = $this->summary['taxable']['total'] / $this->summary['taxable']['count'];
                $this->summary['non_taxable']['average'] = $this->summary['non_taxable']['total'] / $this->summary['non_taxable']['count'];
                return $this->summary;
            }

        };
```

Now use the custom `Collector` class to perform the reducing operation.

```php

$details = Iterators::from(build_items())
    ->peek(static function(TangibleItem $item): void { echo "Collecting $item...\n"; })
    ->collect($collector);

print_r($details);

```

Expected Output

```text
Collecting Flour...
Collecting Candy Bar...
Collecting Soda 2L...
Collecting Milk...
Array
(
    [taxable] => Array
        (
            [count] => 2
            [subtotal] => 4.94
            [total] => 5.32045
            [average_before_tax] => 2.47
            [average] => 2.660225
        )

    [non_taxable] => Array
        (
            [count] => 2
            [total] => 14.98
            [average] => 7.49
        )

)
```

### Joining

The joining collector is used to reduce and iterator to a string by joining its elements by a string with an optional 
prefix and/or suffix.

```php
<ul>
<?php
echo Iterators::from(build_items())
    ->collect(Collectors::joining(prefix: "\t<li>", suffix: "</li>\n"));
?>
</ul>
```

```html
<ul>
	<li>Flour</li>
	<li>Candy Bar</li>
	<li>Soda 2L</li>
	<li>Milk</li>
</ul>
```

### GroupBy

Grouping elements of an iterator is done by passing a function which is responsible for generating the key or offset 
for the group. 

Group the `TangibleItem` by whether it is tax or not.
```php
$group  = Iterators::from(build_items())
    ->collect(Collectors::groupBy(fn(TangibleItem $item): string => $item->taxable() ? 'tax' : 'no_tax'));
print_r($group);
```

Expected Output
```text
Array
(
    [no_tax] => Array
        (
            [0] => Nasumilu\Iterators\Tests\TangibleItem Object
                (
                    [name] => Flour
                    [value] => 9.99
                    [tax] => 
                )

            [1] => Nasumilu\Iterators\Tests\TangibleItem Object
                (
                    [name] => Milk
                    [value] => 4.99
                    [tax] => 
                )

        )

    [tax] => Array
        (
            [0] => Nasumilu\Iterators\Tests\TangibleItem Object
                (
                    [name] => Candy Bar
                    [value] => 2.95
                    [tax] => 0.075
                )

            [1] => Nasumilu\Iterators\Tests\TangibleItem Object
                (
                    [name] => Soda 2L
                    [value] => 1.99
                    [tax] => 0.08
                )

        )

)
```

## Learn More
- [Back to Readme](../README.md)
- [How to use Streams?](streams.md)
- [How to create custom a Collector?](./collector.md)