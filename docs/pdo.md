# How to iterator over PDOStatement?

Your application will almost need to interact with a database and there is no better "_basic_" means for action than
PHP Data Objects (PDO). Since the `PDOStatement` implements the `IteratorAggregate` they may be used to construct a 
`FunctionalIterator` with out any additional requirements.

## Example

First, obtain a connection to the database using one of the many [PDO Drivers](https://www.php.net/manual/en/pdo.drivers.php).

```php
$pdo = new PDO("mysql:host=localhost;dbname=world", 'my_user', 'my_password');
```

Depending on your use case by default a `PDOStatement` when directly iterated will fetch the data indexed by both the
columns ordinal and name (e.g. `PDO::FETCH_BOTH`). In preceding examples the desired output is just the column
ordinal value, (e.g. `PDO::FETCH_NUM`), to achieve this set the `PDO` attribute.

```php
// $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
$pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_NUM);
```

Here is a quick fixture class which represents a person. In this simple use case the results from a prepared statement
will be mapped to a `Person` and then serialized to JSON using the `Person::jsonSerialize` method.

```php
// Person Class
final readonly class Person implements JsonSerializable
{

    public DateTimeInterface $dob;

    /**
     * @throws \Exception
     */
    public function __construct(public int               $id,
                                public string            $firstName,
                                public string            $lastName,
                                DateTimeInterface|string $dob) {

        if (is_string($dob)) {
            $dob = new DateTime($dob);
        }
        $this->dob = $dob;
    }

    public function jsonSerialize() : array {
        return [
            'id' => $this->id,
            'name' => $this->firstName . ' ' . $this->lastName,
            'age' => $this->getAge()
        ];
    }

    public function getAge(): int
    {
        $now = new DateTime('now');
        return $now->diff($this->dob)->y;
    }
}
```

Next create the prepared statement bind the necessary values or parameters and execute. Nothing groundbreaking so
far...

```php
$sql = 'select id, first_name, last_name, dob from person where dob > :dob';
$stmt = $pdo->prepare($sql);
$stmt->bindValue('dob', 32, PDO::PARAM_INT);
$stmt->execute();
```

Notice that the person table's columns are _lower_snake_case_ while the `Person` class use lowerCamelCase for the 
constructor arguments. There are many ways to map the database keys to the properties but for the sake of brevity we will
utilize `array_combine`.

The arrow function makes for handy work ensuring mapping anonymous function has scope to use the `$properties`.

```php
$properties = ['id', 'firstName', 'lastName', 'dob'];
$personMapper = fn(?array $row): Person|null => !$row ? null : new Person(...array_combine($properties, $row));
```

First using a `foreach` loop, not using a mapping function.

```php

$people = [];
foreach($stmt as $row) {
    $people[] = new Person(...array_combine($properties, $row));
}
echo json_encode($people, JSON_PRETTY_PRINT);
```

Now, using `array_map` and `iterator_to_array` as a one-liner. 

```php
echo json_encode(array_map($personMapper(...), iterator_to_array($stmt)), JSON_PRETTY_PRINT);
```

Last, using the `FunctionalIterator` interface,

```php
Iterators::from($stmt)
    ->map($personMapper(...))
    ->stream(Streams::json(JSON_PRETTY_PRINT));
```

## Learn More
- [Back to Readme](../README.md)
- [How to use Streams?](streams.md)
- [How to iterator over PDOStatement?](./pdo.md)