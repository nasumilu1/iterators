# How to use Streams?

A stream is similar to a `Collector` except it writes the iterators current element to a resource handler using an 
instance of `Stream`. The `Stream` class is a control structure which calls an optional beginner `Closure` before the 
first iterator, a stream `Closure` for each iteration and finally an optional finish `Closure`. 


## Built-in Streams

There are two built-in streams JSON and CSV. By default, they both stream to `stdout` unless provide with a valid resource
as context.

### JSON

Streaming an iterator of elements to `stdout` as JSON.
```php
Iterators::from($items)->stream(Streams::json());
```

The JSON stream allows valid options similar to the `json_endode` function, to pretty print the JSON stream:

```php
Iterators::from($items)->stream(Streams::json(JSON_PRETTY_PRINT));
```

Finally, send the JSON to a file
```php
$resource = fopen($path, 'w');
Iterators::from($items)->stream(Streams::json(context: $resource));
fclose($resource);
```

### CSV 

The CSV stream has similar behavior as the JSON stream except its allowed arguments are similar to the `fputcsv`
function.

```php
Iterators::from($items)->stream(Streams::csv());
```
For a tab delimited use:

```php
Iterators::from($items)->stream(Streams::csv("\t"));
```

And finally save the stream to a file,

```php
$resource = fopen($path, 'w')
Iterators::from($items)->stream(Streams::csv("\t", context: $resource));
fclose($resource);
```

## Custom Streams

Fist a class which represent a point-of-interest.

```php
class PointOfInterest implements JsonSerializable
{

    public function __construct(private string $name,
                                private float $x,
                                private float $y)
    {
    }

    public function jsonSerialize(): array
    {
        return [
            'type' => 'Feature',
            'geometry' => [
                'type' => 'Point',
                'coordinates' => [$this->x, $this->y]
            ],
            'properties' => [
                'name' => $this->name
            ]
        ];
    }
}
```

Initialize an iterator containing `PointOfInterest` and stream as GeoJSON using a `Stream`,

```php
$stream = Streams::of(
    function (mixed $value): void {
        if ($this->first ?? true) {
            $this->first = false;
            echo json_encode($value);
            return;
        }
        echo ',' . json_encode($value);
    },
    function (): void {
        echo "{ \"type\": \"FeatureCollection\", \"features\": [";
    },
    function (): void {
        echo ']}';
    }
);
$items = [
    new PointOfInterest('Walt Disney World Resort', -81.5731904, 28.3771289),
    new PointOfInterest('Disnyland Park', -117.9215491, 33.8120962),
    new PointOfInterest('Disney California Adventure Park', -117.9243055, 33.8062236),
    new PointOfInterest('Epcot', -81.5519783, 28.3764734)
];

Iterators::from($items)->stream($stream);
```