<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

use Closure;
use Iterator;
use OuterIterator;

/**
 * The CallbackIterator class the base class that implements an OuterIterator interface and contains reference to a
 * `callable`.
 *
 * Subclasses are responsible for implementing the {@link Iterator::current()} method and applying the referenced
 * callback.
 *
 * @see MapIterator
 * @see PeekIterator
 */
abstract class CallbackIterator implements OuterIterator
{

    /** The referenced callback function. */
    protected $callback;

    /**
     * Constructor method for the class.
     *
     * @param Iterator $iterator The internal iterator to be used.
     * @param callable $callback The callback function applied by subclasses.
     */
    public function __construct(protected readonly Iterator $iterator, callable $callback)
    {
        $this->callback = $callback;
    }

    /**
     * {@inheritDoc}
     */
    public function next(): void
    {
        $this->iterator->next();
    }

    /**
     * {@inheritDoc}
     */
    public function key(): mixed
    {
        return $this->iterator->key();
    }

    /**
     * {@inheritDoc}
     */
    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    /**
     * {@inheritDoc}
     */
    public function rewind(): void
    {
        $this->iterator->rewind();
    }

    /**
     * {@inheritDoc}
     */
    public function getInnerIterator(): ?Iterator
    {
        return $this->iterator;
    }
}