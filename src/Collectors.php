<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

use Closure;

final class Collectors
{

    /**
     * Groups the elements of a collection based on a given classifier function.
     *
     * @param callable $classifier The function used to classify the elements.
     *
     * @return Collector The collector object used to collect the grouped elements.
     */
    public static function groupBy(callable $classifier): Collector
    {
        return new class($classifier) implements Collector {

            private array $groups = [];
            public function __construct(private readonly Closure $classifier)
            {
            }

            public function accumulate(mixed $value, mixed $key): void
            {
                $key = call_user_func($this->classifier, $value, $key);
                if (!array_key_exists($key, $this->groups)) {
                    $this->groups[$key] = [];
                }
                $this->groups[$key][] = $value;
            }

            public function finisher(): array
            {
                return $this->groups;
            }
        };
    }

    /**
     * Creates a Collector instance with a joining operation.
     *
     * This method creates a Collector instance that performs a joining operation. The joining operation concatenates
     * the elements of a stream into a single string. The elements are concatenated using the specified delimiter,
     * prefix, and suffix.
     *
     * @param string $delimiter (optional) The delimiter used to separate the elements in the resulting string. Default
     * is an empty string.
     * @param string|bool $prefix (optional) The prefix to be added before each element in the resulting string. It can
     * be a string or a boolean value. If true, the key of each element will
     * be used as the prefix. Default is an empty string.
     * @param string|bool $suffix (optional) The suffix to be added after each element in the resulting string. It can
     * be a string or a boolean value. If true, the key of each element will be used as the suffix. Default is an empty
     * string.
     *
     * @return Collector A Collector instance that performs the joining operation.
     */
    public static function joining(string $delimiter = '', string|bool $prefix = '', string|bool $suffix = ''): Collector
    {
        return new class($delimiter, $prefix, $suffix) implements Collector {

            private string $str = '';

            public function __construct(private readonly string $delimiter,
                                        private readonly string|bool $prefix,
                                        private readonly string|bool $suffix)
            {
            }

            public function accumulate(mixed $value, mixed $key): void
            {
                $prefix = (true === $this->prefix) ? $key : $this->prefix;
                $suffix = (true === $this->suffix) ? $key : $this->suffix;
                $this->str .= $this->delimiter . $prefix . $value . $suffix;
            }

            public function finisher(): mixed
            {
                return ltrim($this->str, $this->delimiter);
            }
        };
    }

    /**
     * Creates a Collector instance with a minimum-by operation.
     *
     * This method creates a Collector instance that performs a minimum-by operation. The minimum-by operation finds the
     * minimum element in a stream based on a given comparator.
     *
     * @param callable $comparator The comparator used to compare the elements and determine the minimum element.
     *
     * @return Collector The Collector instance which finds the minimum value
     */
    public static function min(callable $comparator): Collector
    {
        return new class($comparator) implements Collector {
            private mixed $min = null;

            public function __construct(private readonly Closure $comparator)
            {
            }

            function accumulate(mixed $value, mixed $key): void
            {
                if (null === $this->min) {
                    $this->min = $value;
                } elseif (call_user_func($this->comparator, $this->min, $value) >= 0) {
                    $this->min = $value;
                }
            }

            public function finisher(): mixed
            {
                return $this->min;
            }

        };
    }

    /**
     * Returns a Collector that finds the maximum element according to the provided comparator.
     *
     * @param callable $comparator The comparator function used to compare elements.
     * @return Collector The Collector instances that finds the maximum element.
     */
    public static function max(callable $comparator): Collector
    {
        return new class($comparator) implements Collector {
            private mixed $max = null;

            public function __construct(private readonly Closure $comparator)
            {
            }

            function accumulate(mixed $value, mixed $key): void
            {
                if (null === $this->max) {
                    $this->max = $value;
                } elseif (call_user_func($this->comparator, $this->max, $value) <= 0) {
                    $this->max = $value;
                }
            }

            function finisher(): mixed
            {
                return $this->max;
            }
        };
    }

    /**
     * Returns a Collector that calculates the sum of all elements.
     *
     * @return Collector The Collector instance that calculates the sum.
     */
    public static function sum(): Collector
    {
        return new class() implements Collector {
            private int|float $sum = 0;
            public function accumulate(mixed $value, mixed $key): void {
                $this->sum += $value;
            }
            public function finisher(): int|float
            {
                return $this->sum;
            }
        };
    }

    /**
     * Returns a Collector that counts the number of elements collected.
     *
     * @return Collector The Collector instance that counts the number of elements.
     */
    public static function count(): Collector
    {
        return new class() implements Collector {

            private int $count = 0;
            public function accumulate(mixed $value, mixed $key): void
            {
                $this->count++;
            }

            public function finisher(): int
            {
                return $this->count;
            }
        };
    }

    /**
     * Returns a Collector that calculates the average of the elements.
     *
     * @return Collector The Collector object that calculates the average.
     */
    public static function average(): Collector
    {
        return new class() implements Collector {
            private int $count = 0;
            private float|int $sum = 0;

            public function accumulate(mixed $value, mixed $key): void
            {
                $this->count++;
                $this->sum += $value;
            }

            public function finisher(): int|float {
                return 0 != $this->count ? $this->sum / $this->count : 0;
            }
        };
    }
}