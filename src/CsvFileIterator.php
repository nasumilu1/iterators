<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

/**
 * Class CsvFileIterator
 *
 * CsvFileIterator is a subclass of FileIterator that allows iterating over a CSV file.
 */
class CsvFileIterator extends FileIterator
{
    private readonly string $separator;
    private readonly string $enclosure;
    private readonly string $escape;

    /**
     * Constructs an instance of the class.
     *
     * @param string $filename The filename to open and read.
     * @param array $options The optional parameters to customize the behavior of the class.
     *   - separator (string): The field separator character. Defaults to ','.
     *   - enclosure (string): The field enclosure character. Defaults to '"'.
     *   - escape (string): The escape character for special characters within fields. Defaults to '\\'.
     *   - use_include_path (bool): Whether to search for the file in the include path. Defaults to false.
     *
     * @return void
     */
    public function __construct(string $filename, array $options = [])
    {
        $this->separator = $options['separator'] ?? ',';
        $this->enclosure = $options['enclosure'] ?? '"';
        $this->escape = $options['escape'] ?? '\\';
        parent::__construct($filename, $options['use_include_path'] ?? false);
    }

    /**
     * Reads the next line of data from the file and returns it as an array.
     *
     * @return array|bool The next line of data as an array, or false if there is no more data.
     */
    protected function readNext(): array|bool
    {
        $data = parent::readNext();
        if ($data) {
            $data = str_getcsv($data, $this->separator, $this->enclosure, $this->escape);
        }
        return $data;
    }
}