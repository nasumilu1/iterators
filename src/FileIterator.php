<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

use InvalidArgumentException;
use Iterator;

/**
 * Class FileIterator
 *
 * An iterator that allows iterating over the contents of a file line by line.
 */
class FileIterator implements Iterator
{

    private $handler;
    private int $position = 0;

    private mixed $current;

    /**
     * Constructs the object. Opens a file for reading and sets the initial value of the current element.
     *
     * @param string $filename The path to the file to be opened.
     * @param bool $use_include_path [optional] Whether to search for the file in the include path. Default is false.
     *
     * @throws InvalidArgumentException If the file fails to open.
     */
    public function __construct(string $filename, bool $use_include_path = false)
    {
        $this->handler = fopen($filename, 'r', $use_include_path);
        if (false === $this->handler) {
            throw new InvalidArgumentException("Failed to open $filename!");
        }
        $this->current = $this->readNext();
    }

    /**
     * Reads the next line from the file.
     *
     * @return mixed The next line from the file, or false if there are no more lines.
     */
    protected function readNext(): mixed
    {
        return fgets($this->handler);
    }

    /**
     * Destructor for the class.
     *
     * Closes the file handler if it exists.
     *
     * @return void
     */
    public function __destruct()
    {
        if($this->handler) {
            fclose($this->handler);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function current(): mixed
    {
        return $this->current;
    }

    /**
     * {@inheritDoc}
     */
    public function next(): void
    {
        $this->current = $this->readNext();
        $this->position++;
    }

    /**
     * {@inheritDoc}
     */
    public function key(): int
    {
        return $this->position;
    }

    /**
     * {@inheritDoc}
     */
    public function valid(): bool
    {
        return false !== $this->current;
    }

    /**
     * {@inheritDoc}
     */
    public function rewind(): void
    {
        fseek($this->handler, 0);
        $this->position = 0;
        $this->current = $this->readNext();
    }
}