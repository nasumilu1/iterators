<?php
/*
 *  Copyright 2025 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

use Closure;
use Iterator;
use OuterIterator;
use RecursiveIterator;

/**
 * FlatMapIterator is an implementation of RecursiveIterator that applies a provided callback
 * to each element of the iterator.
 */
class FlatMapIterator implements RecursiveIterator, OuterIterator
{

    /** The referenced callback function. */
    protected ?Closure $callback = null;

    /**
     * Constructor method for the class.
     *
     * @param Iterator $iterator The internal iterator to be used.
     * @param callable|null $callback The callback function applied by subclasses.
     */
    public function __construct(protected readonly Iterator $iterator, ?callable $callback = null)
    {
        $this->callback = $callback;
    }

    /**
     * {@inheritDoc}
     */
    public function hasChildren(): bool
    {
        return is_iterable($this->iterator->current());
    }

    /**
     * {@inheritDoc}
     */
    public function getChildren(): ?RecursiveIterator
    {
        return new self(Iterators::from($this->iterator->current()), $this->callback);
    }

    /**
     * {@inheritDoc}
     */
    public function next(): void
    {
        $this->iterator->next();
    }

    /**
     * {@inheritDoc}
     */
    public function key(): mixed
    {
        return $this->iterator->key();
    }

    /**
     * {@inheritDoc}
     */
    public function valid(): bool
    {
        return $this->iterator->valid();
    }

    /**
     * {@inheritDoc}
     */
    public function rewind(): void
    {
        $this->iterator->rewind();
    }

    /**
     * {@inheritDoc}
     */
    public function getInnerIterator(): ?Iterator
    {
        return $this->iterator;
    }

    /**
     * {@inheritDoc}
     */
    public function current(): mixed
    {
        if (null === $this->callback) {
            return $this->iterator->current();
        }
        return call_user_func($this->callback,  $this->iterator->current(), $this->iterator->keY());
    }
}