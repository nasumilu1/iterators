<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

use Exception;
use OuterIterator;
use RecursiveIteratorIterator;

/**
 * This interface extends the {@link OuterIterator} interface and defines a set of methods that provide a fluent and
 * functional interface for working with {@link Iterators}.
 */
interface FunctionalIterator extends OuterIterator
{

    /**
     * Applies a given callable function to each element of the array.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $iterator
     *     ->foreach(function (string $value, int $key): void {
     *         echo "Key: $key; Value: $value" . PHP_EOL;
     *     });
     * </code>
     *
     * @param callable $fn The function to apply to each element. The function should accept the current element as
     * its first argument and the key as the second argument.
     *
     * @return void
     */
    public function foreach(callable $fn): void;

    /**
     * Peeks at the iterators current element utilizing the provided callback, allowing to inspect or to perform
     * intermediary operations.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $logger = new Logger('app');
     * $log->pushHandler(new StreamHandler('php://stdout', Logger::WARNING));
     *
     * $bad_word = '%&*#';
     * $iterator
     *     ->peek(function (string $value, int $key) use ($logger, $bad_word): void {
     *         if ($value === $bad_word) {
     *             $logger->warning("Bad word ($value) detected at offset $key!");
     *         }
     *     })
     *     ->filter(fn(string $value): bool) => $value !== $bad_word)
     *     ->foreach(function(string $value): void { echo $value; });
     * </code>
     *
     * @param callable $fn The function to apply to the current element. The function should accept the current element
     * as its first argument and the key as the second argument.
     *
     * @return FunctionalIterator An instance of FunctionalIterator which applies the peek callback function to each
     * element.
     * @throws Exception
     */
    public function peek(callable $fn): FunctionalIterator;

    /**
     * Maps the iterators current element utilizing the provided callback.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $iterator
     *     ->map(static fn(int $value): int => pow($value, 2))
     *     ->foreach(function(int $value): void {
     *         echo $value;
     *     });
     * </code>
     *
     * @param callable $fn The function to apply to each element. The function should accept the current element as
     * its first argument and the key as the second argument.
     *
     * @return FunctionalIterator An instance of FunctionalIterator which applies mapping function to each element.
     * @throws Exception
     */
    public function map(callable $fn): FunctionalIterator;


    /**
     * Applies a given callable function to each element of the array and flattens the result by traversing nested arrays or iterables.
     *
     * @param callable|null $fn The function to apply to each element. The function should accept the current element as its first argument and the key as the second argument. The function should return an array or iterable for flattening.
     * @param int $mode Optional mode to determine how the iterator traverses the structure. Defaults to RecursiveIteratorIterator::LEAVES_ONLY.
     * @param int $flags Optional flags used to control iterator behavior. Defaults to 0.
     *
     * @return FunctionalIterator A new FunctionalIterator instance with the flattened and transformed elements.
     */
    public function flatMap(?callable $fn = null, int $mode = RecursiveIteratorIterator::LEAVES_ONLY, int $flags = 0): FunctionalIterator;


    /**
     * Filters the elements of the iterator based on a given predicate function.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $iterator->filter(static fn(int $value, int $key): bool => $value > 5);
     * </code>
     *
     * @param callable $predicate The predicate function used to evaluate each element of the array. The function should
     * accept the current element as its first argument and the key as the second argument, and return a boolean value
     * indicating whether the element should be included in the filtered result.
     *
     * @return FunctionalIterator An instance of FunctionalIterator applies the predicate callback.
     * @throws Exception
     */
    public function filter(callable $predicate): FunctionalIterator;


    /**
     * Checks if every element in the array satisfies the given predicate.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * if ($iterator->every(static fn(int $value, int $key): bool => $value > 5)) {
     *     echo 'All the values are greater than 5';
     * }
     * </code>
     *
     * @param callable $predicate A function that accepts an element and returns a boolean value indicating whether the
     * element satisfies a certain condition.
     *
     * @return bool True if every element satisfies the predicate, false otherwise.
     */
    public function every(callable $predicate): bool;

    /**
     * Checks if at least one element in the array satisfies the given predicate function.
     *
     * <h4>Basic Usage</h4>
     * <code>
     *  if ($iterator->some(fn(int $value, int $key): bool => $value > 5)) {
     *      echo 'At least one of the values is greater than 5';
     *  }
     * </code>
     *
     * @param callable $predicate The function to call for each element. The function should accept the current element
     * as its first argument and return a boolean value indicating whether the element satisfies the condition.
     *
     * @return bool True if at least one element satisfies the predicate, false otherwise.
     */
    public function some(callable $predicate): bool;

    /**
     * Searches for an element in the iterator that satisfies the given predicate.
     *
     * <blockquote style="border-left: 2px solid white;padding-left:5px;">
     *  IMPORTANT: Returns the first element which meets the predicate's criteria. If you need all elements which
     *  meet a predicate's criteria use {@link FunctionalIterator::filter()}.
     * </blockquote>
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $person = $iterator->find(static fn(Person $person): bool => $person->getAge() > 21);
     * if (null !== $person) {
     *     echo $person->getName() . ', please have a beer on the house!';
     * }
     * </code>
     *
     * @param callable $predicate The predicate function used to test each element. The function should accept the
     * current element as its first argument and the key as the second argument. The function should return a boolean
     * value. If no element is found which meets the predicate's criteria the default values is returned (default: null).
     *
     * @param mixed $default (optional) The default value to return if no element in the array satisfies the predicate.
     * Defaults to null.
     *
     * @return mixed|null Returns the first element in the array that satisfies the predicate or the default value if
     * no element satisfies the predicate.
     *
     * @throws Exception
     */
    public function find(callable $predicate, mixed $default = null): mixed;


    /**
     * Returns the first element of the iterator, or a default value if empty.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $result = $iterator
     *     ->filter(static fn(int $value): bool => $value > 5)
     *     ->first(5);
     * </code>
     *
     * @param mixed $default (Optional) The default value to return if the array is empty. Default is null.
     *
     * @return mixed The first element in the iterator or the default value if empty.
     */
    public function first(mixed $default = null): mixed;


    /**
     * Returns the last element of the iterator or a default value if empty.
     *
     *  <h4>Basic Usage</h4>
     *  <code>
     *  $result = $iterator
     *      ->filter(static fn(int $value): bool => $value > 5)
     *      ->last(5);
     *  </code>
     *
     * @param mixed $default (Optional) The value to be returned if the iterator is empty. Defaults to null.
     *
     * @return mixed The last element in the iterator or the default value if empty.
     */
    public function last(mixed $default = null): mixed;

    /**
     * Returns all the values of an array.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $result = $iterator
     *     ->filter(static fn(int $value): bool => $value > 5)
     *     ->values(static fn(int $a, int $b): int $a <=> $b);
     * </code>
     *
     * @param callable|bool $sort Determines whether the iterator values should be sorted. If a callable function is
     * provided, the function is used for sorting the values. If false, the original order of values is preserved.
     * Default is false.
     *
     * @param bool $preserve_keys Determines whether the numeric keys of the array should be preserved or re-indexed.
     * If set to true, the keys are preserved. If set to false, the keys are re-indexed. Default is true.
     *
     * @return array An array containing all the values of the iterator.
     */
    public function values(callable|bool $sort = false, bool $preserve_keys = true): array;

    /**
     * Searches for an element in the array that satisfies the given predicate function and returns the element's
     * offset, otherwise returns the default value.
     *
     * @param callable $predicate The function used to test each element in the array. The function should accept
     * the element as its first argument and offset as the second argument. The callable MUST return a boolean value
     * indicating whether the element matches the search criteria.
     *
     * @param mixed $default The default value to return if no element in the array satisfies the predicate.
     *
     * @return mixed|null The first element in the array that satisfies the predicate, or the default value if no
     * such element is found.
     */
    public function search(callable $predicate, mixed $default = null): mixed;

    /**
     * Creates an iterator that generates a sequence of values from $start to $end with a specified $step.
     *
     * @param int|float|string $start The starting value of the range.
     * @param int|float|string $end The ending value of the range.
     * @param int|float $step The step to increment or decrement the range by.
     *
     * @return FunctionalIterator An iterator that generates the range of values.
     * @link https://www.php.net/manual/en/function.range.php
     */
    public static function range(int|float|string $start, int|float|string $end, int|float $step = 1): FunctionalIterator;

    /**
     * Applies a given reducer function to each element of the array and returns a single value.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $result = $iterator
     *     ->reduce(function (mixed $accumulator, mixed $value, int $key) {
     *         return $accumulator + $value;
     *     });
     * </code>
     *
     * @param callable $reducer The reducer function to apply to each element. The function should accept an accumulator,
     * the current element, and the key as arguments, and return the updated accumulator.
     * @param mixed|null $initial The initial value of the accumulator. If not provided, the first element of the array
     * will be used as the initial value.
     *
     * @return mixed The final value of the accumulator after applying the reducer function to each element.
     */
    public function reduce(callable $reducer, mixed $initial = null): mixed;

    /**
     * Delays the execution of the iterator for a given amount of time.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $iterator
     *     ->delay(2) // Delays execution for 2 seconds
     *     ->foreach(function (string $value, int $key): void {
     *         echo "Key: $key; Value: $value" . PHP_EOL;
     *     });
     * </code>
     *
     * @param float|int $delay The amount of time to delay the execution of the iterator, in seconds.
     * Accepts both float and integer values.
     *
     * @return FunctionalIterator The modified iterator with the delay applied.
     */
    public function delay(float | int $delay): FunctionalIterator;

    /**
     * Collects elements using a given collector.
     *
     * <h4>Basic Usage</h4>
     * <code>
     * $result = $iterator->collect(Collectors::joining(' ', 'value_'));
     * </code>
     *
     * or
     *
     * <code>
     * $accumulator = function(mixed $value): void {
     *     // accumulate values here
     * };
     * $finisher = function(): void {
     *     // finishing operation performed (optional)
     * };
     * $result = $iterator->collect(Collectors::of($accumulator, $finisher);
     * </code>
     *
     *
     * @return mixed The result of the collection operation.
     *
     * @see Collector
     * @see Collectors
     */
    public function collect(Collector $collector): mixed;

    /**
     *
     * @param Stream $stream
     * @return void
     */
    public function stream(Stream $stream): void;

}