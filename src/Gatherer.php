<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */
namespace Nasumilu\Iterators;

/**
 * The Gatherer interface represents a mechanism for combining and processing values.
 */
interface Gatherer
{
    /**
     * Combines the given value and key.
     *
     * @param mixed $value The value to be combined.
     * @param mixed $key The key to be combined.
     * @return void
     */
    public function combiner(mixed $value, mixed $key): void;

    /**
     * Execute the finisher operation.
     *
     * @return iterable The result of the finisher operation.
     */
    public function finisher(): iterable;
}