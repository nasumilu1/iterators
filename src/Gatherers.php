<?php

namespace Nasumilu\Iterators;

use Closure;

final class Gatherers
{

    /**
     * Create a fixed-size window Gatherer.
     *
     * @param int $size The size of the window.
     * @param bool $preserve_keys Whether to preserve the keys of the input array.
     *
     * @return Gatherer Returns a new windowFixed object.
     */
    public static function windowFixed(int $size, bool $preserve_keys = false): Gatherer {
        return new class($size, $preserve_keys) implements Gatherer {

            private array $gathered = [];
            private array $gathering = [];

            public function __construct(private readonly int $size, private readonly bool $preserve)
            {
            }

            public function combiner(mixed $value, mixed $key): void
            {
                $count = count($this->gathering);
                $this->gathering[$this->preserve ? $key : $count] = $value;
                if ($count + 1 === $this->size) {
                    $this->gathered[] = $this->gathering;
                    $this->gathering = [];
                }
            }

            public function finisher(): array
            {
                if (!empty($this->gathering)) {
                    $this->gathered[] = $this->gathering;
                }
                return $this->gathered;
            }
        };
    }

    /**
     * Create a sliding window Gatherer.
     *
     * @param int $size The size of the window.
     * @param bool $preserve_keys Whether to preserve the keys of the input array.
     *
     * @return Gatherer Returns a new windowSliding object.
     */
    public static function windowSliding(int $size, bool $preserve_keys = false): Gatherer {
        return new class($size, $preserve_keys) implements Gatherer {

            private array $gathered = [];
            private array $gathering = [];

            public function __construct(private readonly int $size, private readonly bool $preserve)
            {
            }

            public function combiner(mixed $value, mixed $key): void
            {
                $count = count($this->gathering);
                $this->gathering[$this->preserve ? $key : $count] = $value;
                if ($count + 1 === $this->size) {
                    $this->gathered[] = $this->gathering;
                    array_shift($this->gathering);
                }
            }

            public function finisher(): array
            {
                if (empty($this->gathered) && !empty($this->gathering)) {
                    $this->gathered[] =  $this->gathering;
                }
                return $this->gathered;
            }
        };
    }

    /**
     * Create a fold Gatherer using a given folder closure to perform the ordered, reduction-like operation.
     *
     * @param callable $folder The closure used for folding.
     *
     * @return Gatherer Returns a new fold object.
     */
    public static function fold(callable $folder): mixed {
        return new class($folder) implements Gatherer {

            private mixed $value = null;
            public function __construct(private readonly Closure $folder)
            {

            }
            public function combiner(mixed $value, mixed $key): void
            {
               $this->value = call_user_func($this->folder, $this->value, $value, $key);
            }

            public function finisher(): iterable
            {
                return is_iterable($this->value) ? $this->value : [$this->value];
            }
        };
    }

}