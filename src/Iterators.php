<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

use AppendIterator;
use ArrayIterator;
use CallbackFilterIterator;
use Exception;
use Iterator;
use IteratorAggregate;
use RecursiveIteratorIterator;

/**
 * The Iterators class is an implementation of the {@link FunctionalIterator} interface. It provides various methods to
 * perform functional-style operations on iterables.
 */
readonly class Iterators implements FunctionalIterator
{

    private function __construct(private Iterator $iterator)
    {
    }

    /**
     * Creates a {@link FunctionalIterator} object from the given iterable or callback function.
     *
     * @param iterable|callable $iterable The iterable or callback used to create an Iteration object from. When
     *                                    provided a callback it MUST return an iterable.
     *
     * @return Iterators The Iteration object created from the iterable.
     * @throws Exception
     */
    public static function from(iterable|callable $iterable): FunctionalIterator
    {
        if (is_callable($iterable)) {
            $iterable = call_user_func($iterable);
        }
        if ($iterable instanceof FunctionalIterator) {
            return $iterable;
        }

        if (is_array($iterable)) {
            $iterable = new ArrayIterator($iterable);
        } else if ($iterable instanceof IteratorAggregate) {
            $iterable = $iterable->getIterator();
        }

        return new Iterators($iterable);
    }

    /**
     * Creates a {@link FunctionalIterator} by concatenates multiple iterables as if a single iterator.
     *
     * @param iterable ...$iterables The iterables to concatenate.
     *                               Can be multiple iterables separated by comma.
     *
     * @return Iterators The concatenated iterator.
     * @throws Exception
     */
    public static function concat(iterable ...$iterables): Iterators
    {
        $iterator = new AppendIterator();
        Iterators::from($iterables)
            ->map(function (iterable $iterable): iterable {
                if (is_array($iterable)) {
                    return new ArrayIterator($iterable);
                }

                if ($iterable instanceof IteratorAggregate) {
                    return $iterable->getIterator();
                }

                return $iterable;
            })->foreach(function (Iterator $iterable) use ($iterator): void {
                $iterator->append($iterable);
            });
        return Iterators::from($iterator);
    }

    /**
     * {@inheritDoc}
     */
    public function foreach(callable $fn): void
    {
        foreach ($this as $key => $value) {
            call_user_func($fn, $value, $key);
        }
    }

    /**
     * {@inheritDoc}
     */
    public function peek(callable $fn): FunctionalIterator
    {
        return Iterators::from(new PeekIterator($this->getInnerIterator(), $fn));
    }

    /**
     * {@inheritDoc}
     */
    public function map(callable $fn): FunctionalIterator
    {
        return Iterators::from(new MapIterator($this->getInnerIterator(), $fn));
    }

    /**
     * {@inheritDoc}
     */
    public function flatMap(?callable $fn = null, int $mode = RecursiveIteratorIterator::LEAVES_ONLY, int $flags = 0): FunctionalIterator
    {
        return Iterators::from(new RecursiveIteratorIterator(new FlatMapIterator($this->getInnerIterator(), $fn)));
    }

    /**
     * {@inheritDoc}
     */
    public function filter(callable $predicate): FunctionalIterator
    {
        return Iterators::from(new CallbackFilterIterator($this->getInnerIterator(), $predicate));
    }

    /**
     * {@inheritDoc}
     */
    public function every(callable $predicate): bool
    {
        foreach ($this as $key => $value) {
            if (!call_user_func($predicate, $value, $key)) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritDoc}
     */
    public function some(callable $predicate): bool
    {
        foreach ($this as $key => $value) {
            if (call_user_func($predicate, $value, $key)) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    public function find(callable $predicate, mixed $default = null): mixed
    {
        foreach($this as $key=>$value) {
            if (call_user_func($predicate, $value, $key)) {
                return $value;
            }
        }
        return $default;
    }

    /**
     * {@inheritDoc}
     */
    public function search(callable $predicate, mixed $default = null): mixed
    {
        foreach($this as $key=>$value) {
            if (call_user_func($predicate, $value, $key)) {
                return $key;
            }
        }
        return $default;
    }

    public static function range(float|int|string $start, float|int|string $end, float|int $step = 1): FunctionalIterator
    {
        return Iterators::from(range($start, $end, $step));
    }

    /**
     * {@inheritDoc}
     */
    public function first(mixed $default = null): mixed
    {
        $this->rewind();
        return $this->current() ?? $default;

    }

    /**
     * {@inheritDoc}
     */
    public function last(mixed $default = null): mixed
    {
        $values = $this->values();
        return end($values) ?? $default;
    }

    /**
     * {@inheritDoc}
     */
    public function values(callable|bool $sort = false, bool $preserve_keys = true): array
    {
        $values = iterator_to_array($this->getInnerIterator(), $preserve_keys);
        if (true === $sort) {
            sort($values);
        }
        if (is_callable($sort)) {
            usort($values, $sort);
        }
        return $values;
    }

    /**
     * {@inheritDoc}
     */
    public function reduce(callable $reducer, mixed $initial = null): mixed
    {
        foreach($this as $key=>$value) {
            $initial = call_user_func($reducer, $initial, $value, $key);
        }
        return $initial;
    }


    /**
     * {@inheritDoc}
     */
    public function delay(float|int $delay): FunctionalIterator
    {
        $fn = function() use ($delay): void {
            if ($delay < 1) {
                usleep($delay * 1000000);
            } else {
                sleep($delay);
            }
        };
        return Iterators::from(new PeekIterator($this->getInnerIterator(), $fn));
    }

    /**
     * {@inheritDoc}
     */
    public function collect(Collector $collector): mixed
    {
        $this->foreach($collector->accumulate(...));
        return $collector->finisher();
    }

    public function stream(Stream $stream): void
    {
        $stream->open();
        $this->foreach($stream->write(...));
        $stream->close();
    }

    public function gather(Gatherer $gatherer): FunctionalIterator {
        $this->foreach($gatherer->combiner(...));
        return Iterators::from($gatherer->finisher());
    }

    /**
     * {@inheritDoc}
     */
    public function current(): mixed
    {
        return $this->getInnerIterator()->current();
    }

    /**
     * {@inheritDoc}
     */
    public function next(): void
    {
        $this->getInnerIterator()->next();
    }

    /**
     * {@inheritDoc}
     */
    public function key(): mixed
    {
        return $this->getInnerIterator()->key();
    }

    /**
     * {@inheritDoc}
     */
    public function valid(): bool
    {
        return $this->getInnerIterator()->valid();
    }

    /**
     * {@inheritDoc}
     */
    public function rewind(): void
    {
        $this->getInnerIterator()->rewind();
    }

    /**
     * {@inheritDoc}
     */
    public function getInnerIterator(): Iterator
    {
        return $this->iterator;
    }

    public static function echo(string $tpl): callable
    {
        return static function(mixed $value, mixed $key) use ($tpl): void {
            echo str_replace(['{{key}}', '{{value}}'], [$key, $value], $tpl);
        };
    }
}