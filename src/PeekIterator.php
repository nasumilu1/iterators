<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

/**
 * This class is an extends the {@link CallbackIterator} to peek at the current iteration's element. The current element
 * <strong>SHALL</strong> remain unmodified.
 *
 * ### Basic Usage
 * ```
 * $array = [1, 2, 3, 4, 5];
 * $callback = function (int $current, int $key): void {
 * echo "Key: " . $key;
 * echo "; Value: " . $current;
 * };
 *
 * $iterator = new ArrayIterator($array);
 * $peekIterator = new PeekIterator($iterator, $callback);
 *
 * foreach ($peekIterator as $key => $value) {
 * // the peek callback with invoke the $callback echoing the current value and key.
 * echo "; Square: " . $value * $value . PHP_EOL;
 * }
 * ```
 *
 * ### Expected Output
 * ```
 * Key: 0; Value: 1; Square: 1
 * Key: 1; Value: 2; Square: 4
 * Key: 2; Value: 3; Square: 9
 * Key: 3; Value: 4; Square: 16
 * Key: 4; Value: 5; Square: 25
 * ```
 */
class PeekIterator extends CallbackIterator
{

    /**
     * {@inheritDoc}
     */
    public function current(): mixed
    {
        $current = $this->iterator->current();
        call_user_func($this->callback, $current, $this->key());
        return $current;
    }

}