<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

/**
 * The Stream interface represents a stream of data that can be opened, written to, and closed.
 */
interface Stream
{
    /**
     * Opens and/or writes initial output to the resource.
     */
    public function open(): void;

    /**
     * Writes a value to a resource along with an optional key.
     *
     * @param mixed $value The value to be written.
     * @param mixed $key (optional) The key to be associated with the value.
     *
     * @return void
     */
    public function write(mixed $value, mixed $key): void;

    /**
     * Closes the current file or stream.
     *
     * @return void
     */
    public function close(): void;

}