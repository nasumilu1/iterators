<?php

namespace Nasumilu\Iterators\Stream;

use InvalidArgumentException;
use Nasumilu\Iterators\Stream;

class CsvStream implements Stream
{
    private bool $first = true;
    private bool $close = false;

    public function __construct(private readonly string $separator,
                                private readonly string $enclosure,
                                private readonly string $escape,
                                private readonly string $eol,
                                private readonly bool   $header,
                                private mixed           $resource = null)
    {
        if ($this->resource && !is_resource($this->resource)) {
            throw new InvalidArgumentException(sprintf(
                'Expected a resource, found %s!',
                is_object($this->resource) ? get_class($this->resource) : gettype($this->resource)
            ));
        }
    }

    public function open(): void
    {
        $this->close = null === $this->resource;
        $this->resource ??= fopen('php://output', 'w');
    }

    public function write(mixed $value, mixed $key): void
    {
        if ($this->first) {
            $this->first = false;
            if ($this->header) {
                fputcsv($this->resource, array_keys($value), $this->separator, $this->enclosure, $this->escape, $this->eol);
            }
        }
        fputcsv($this->resource, $value, $this->separator, $this->enclosure, $this->escape, $this->eol);
    }

    public function close(): void
    {
        if ($this->close) {
            fclose($this->resource);
        }
    }
}