<?php

namespace Nasumilu\Iterators\Stream;

use InvalidArgumentException;
use Nasumilu\Iterators\Stream;

class EchoStream implements Stream
{
    private bool $close = false;

    public function __construct(private mixed $resource = null)
    {
        if ($this->resource && !is_resource($this->resource)) {
            throw new InvalidArgumentException(sprintf(
                'Expected a resource, found %s!',
                is_object($this->resource) ? get_class($this->resource) : gettype($this->resource)
            ));
        }
    }

    public function open(): void
    {
        $this->close = null === $this->resource;
        $this->resource ??= fopen('php://output', 'w');
    }

    public function write(mixed $value, mixed $key): void
    {
        fwrite($this->resource, "$key => ");
        if (is_object($value) && method_exists($value, '__toString')) {
            fwrite($this->resource, (string) $value . "\n");
        }
        else if (is_array($value) || is_object($value)) {
            fwrite($this->resource, print_r($value, true));
        } else if (is_scalar($value)) {
            fwrite($this->resource, $value);
        } else {
            $this->close();
            throw new \InvalidArgumentException("Expected object, array or other scalar value, found " . gettype($value));
        }
    }

    public function close(): void
    {
        if ($this->close) {
            fclose($this->resource);
        }
    }
}