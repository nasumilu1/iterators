<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators\Stream;

use InvalidArgumentException;
use Nasumilu\Iterators\Stream;

/**
 * Class JsonStream
 * Implements the Stream interface and provides functionality to write data as JSON to a stream.
 */
class JsonStream implements Stream
{
    private bool $first = true;
    private bool $close = false;

    /**
     * Constructor for the class.
     *
     * @param int $flags (optional) The flags value. Default is 0.
     * @param int $depth (optional) The depth value. Default is 512.
     * @param resource $resource (optional) The resource value. Default is null.
     *
     * @throws InvalidArgumentException if the provided $resource is not a valid resource.
     */
    public function __construct(private readonly int $flags = 0,
                                private readonly int $depth = 512,
                                private mixed        $resource = null)
    {
        if ($this->resource && !is_resource($this->resource)) {
            throw new InvalidArgumentException(sprintf(
                'Expected a resource, found %s!',
                is_object($this->resource) ? get_class($this->resource) : gettype($this->resource)
            ));
        }
    }

    /**
     * {@inheritDoc}
     */
    public function open(): void
    {
        $this->close = null === $this->resource;
        $this->resource ??= fopen('php://output', 'w');
        fwrite($this->resource, '[');
    }

    public function write(mixed $value, mixed $key): void
    {
        fflush($this->resource);
        $data = json_encode($value, $this->flags, $this->depth);
        if ($this->first ?? true) {
            $this->first = false;
        } else {
            fwrite($this->resource, ',');
            fwrite($this->resource, ($this->flags & JSON_PRETTY_PRINT) === JSON_PRETTY_PRINT ? "\n" : '');
        }
        fwrite($this->resource, $data);
    }

    public function close(): void
    {
        fwrite($this->resource, ']');
        fflush($this->resource);
        if ($this->close) {
            fclose($this->resource);
        }
    }

}