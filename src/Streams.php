<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators;

use Nasumilu\Iterators\Stream\CsvStream;
use Nasumilu\Iterators\Stream\EchoStream;
use Nasumilu\Iterators\Stream\JsonStream;

/**
 * Class Streams
 *
 * This class provides static methods to create different types of streams.
 */
final class Streams
{

    /**
     * Generate a JSON stream for writing data in a specific format.
     *
     * @param int $flags The bitmask of JSON encoding options.
     * @param int $depth The maximum depth of the encoding.
     * @param resource|null $resource The resource to write the JSON stream to. Set to null to use 'php://stdout' as the default output stream.
     *
     * @return Stream The JSON stream for writing data.
     */
    public static function json(int $flags = 0, int $depth = 512, mixed $resource = null): Stream
    {
        return new JsonStream($flags, $depth, $resource);
    }

    /**
     * Generate a CSV stream for writing data in CSV format.
     *
     * @param string $separator The character used to separate fields in CSV. Default is ','.
     * @param string $enclosure The character used to enclose values in CSV. Default is '"'.
     * @param string $escape The character used to escape special characters in CSV. Default is '\'.
     * @param string $eol The end of line character used in CSV. Default is '\n'.
     * @param bool $header Whether to include a header row in the CSV. Default is true.
     * @param resource|null $resource The resource to write the CSV stream to. Set to null
     *                               to use 'php://stdout' as the default output stream.
     *
     * @return Stream The CSV stream for writing data.
     */
    public static function csv(string $separator = ",",
                               string $enclosure = "\"",
                               string $escape = "\\",
                               string $eol = "\n",
                               bool   $header = true,
                               mixed  $resource = null): Stream
    {
        return new CsvStream($separator, $enclosure, $escape, $eol, $header, $resource);
    }

    /**
     * Retrieves the echo stream.
     *
     * @return Stream The echo stream object.
     */
    public static function echo(mixed  $resource = null) : Stream
    {
        return new EchoStream($resource);
    }
}