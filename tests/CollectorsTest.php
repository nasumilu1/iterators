<?php

namespace Nasumilu\Iterators\Tests;

use Exception;
use Generator;
use Nasumilu\Iterators\CallbackIterator;
use Nasumilu\Iterators\Collector;
use Nasumilu\Iterators\Collectors;
use Nasumilu\Iterators\Iterators;
use Nasumilu\Iterators\MapIterator;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;
use TypeError;


#[CoversClass(Iterators::class)]
#[CoversClass(Collector::class)]
#[CoversClass(Collectors::class)]
#[CoversClass(CallbackIterator::class)]
#[CoversClass(MapIterator::class)]
class CollectorsTest extends TestCase
{

    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Collectors::joining test')]
    public function joining(): void
    {
        $expected = 'value_6_value_8_value_9_value_7_';
        $iterator = Iterators::from(['six' => 6, 'eight' => 8, 'nine' => 9, 'seven' => 7]);
        $this->assertEquals($expected, $iterator->collect(Collectors::joining(prefix: 'value_', suffix: '_')));
    }


    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Collectors::minBy test')]
    public function min(): void
    {
        $min = static fn(int $v1, int $v2): int => $v1 <=> $v2;
        $expected = 6;
        $iterator = Iterators::from(['eight' => 8, 'six' => 6, 'nine' => 9, 'seven' => 7]);
        $this->assertEquals($expected, $iterator->collect(Collectors::min($min)));
    }


    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Collectors::max test')]
    public function maxBy(): void
    {
        $max = static fn(int $v1, int $v2): int => $v1 <=> $v2;
        $expected = 9;
        $iterator = Iterators::from(['six' => 6, 'eight' => 8, 'nine' => 9, 'seven' => 7]);
        $this->assertEquals($expected, $iterator->collect(Collectors::max($max)));
    }

    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Collectors::sum test')]
    public function sum(): void
    {
        $values = ['six' => 6, 'eight' => 8, 'nine' => 9, 'seven' => 7];
        $expected = array_sum($values);
        $iterator = Iterators::from($values);
        $this->assertEquals($expected, $iterator->collect(Collectors::sum()));
    }

    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Collectors::sum test')]
    public function nonNumericSum(): void
    {
        $values = [6 => 'six', 'eight' => 8, 'nine' => 9, 'seven' => 7];
        $this->expectException(TypeError::class);
        $iterator = Iterators::from($values);
        $iterator->collect(Collectors::sum());
    }

    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Collectors::count test')]
    public function countCollector(): void
    {
        $values = ['six' => 6, 'eight' => 8, 'nine' => 9, 'seven' => 7];
        $iterator = Iterators::from($values);
        $this->assertEquals(count($values), $iterator->collect(Collectors::count()));
    }


    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Collectors::count test')]
    public function average(): void
    {
        $values = ['six' => 6, 'eight' => 8, 'nine' => 9, 'seven' => 7];
        $expected = array_sum($values) / count($values);
        $iterator = Iterators::from($values);
        $this->assertEquals($expected, $iterator->collect(Collectors::average()));
    }


    #[Test]
    #[TestDox('Collectors::groupBy')]
    public function groupBy(): void
    {
        $fixture1 = [1, 2, 3, 4, 5, 6, 7];
        $fixture2 = [8, 9, 10, 11, 12, 13, 14, 15];

        $values = Iterators::concat($fixture1, $fixture2)
            ->collect(Collectors::groupBy(static fn(int $value): int => $value & 1));
        $this->assertTrue(Iterators::from($values[0])->every(fn(int $value): bool => !($value & 1)));
        $this->assertTrue(Iterators::from($values[1])->every(fn(int $value): bool => $value & 1));
    }

    #[Test]
    #[TestDox('Readme example')]
    public function taxsummary()
    {

        $generator = function (): Generator {
            yield new TangibleItem('Flour', 9.99);
            yield new TangibleItem('Candy Bar', 2.95, 0.075);
            yield new TangibleItem('Soda 2L', 1.99, 0.08);
            yield new TangibleItem('Milk', 4.99);
        };

        $collector = new class() implements Collector {
            private array $summary = ['taxable' => ['count' => 0,
                'subtotal' => 0,
                'total' => 0,
                'average_before_tax' => 0,
                'average' => 0,],
                'non_taxable' => ['count' => 0,
                    'total' => 0,
                    'average' => 0,]];

            public function accumulate(mixed $value, mixed $key): void
            {
                if ($value->taxable()) {
                    $this->summary['taxable']['count'] += 1;
                    $this->summary['taxable']['subtotal'] += $value->value;
                    $this->summary['taxable']['total'] += $value->total();
                } else {
                    $this->summary['non_taxable']['count'] += 1;
                    $this->summary['non_taxable']['total'] += $value->total();
                }
            }

            public function finisher(): array
            {
                $this->summary['taxable']['average_before_tax'] = $this->summary['taxable']['subtotal'] / $this->summary['taxable']['count'];
                $this->summary['taxable']['average'] = $this->summary['taxable']['total'] / $this->summary['taxable']['count'];
                $this->summary['non_taxable']['average'] = $this->summary['non_taxable']['total'] / $this->summary['non_taxable']['count'];
                return $this->summary;
            }

        };
        $details = Iterators::from($generator())
            //->peek(static function(TangibleItem $item): void { echo "Collecting $item...\n"; })
            ->collect($collector);
        //print_r($details);
        $this->assertIsArray($details);

        $group  = Iterators::from($generator())
            ->collect(Collectors::groupBy(fn(TangibleItem $item): string => $item->taxable() ? 'tax' : 'no_tax'));
        //print_r($group);
        $this->assertIsArray($group);

    }

}