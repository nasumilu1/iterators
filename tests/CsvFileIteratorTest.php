<?php

namespace Nasumilu\Iterators\Tests;

use Nasumilu\Iterators\CsvFileIterator;
use Nasumilu\Iterators\FileIterator;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\TestWith;
use PHPUnit\Framework\TestCase;

/**
 * CsvFileIteratorTest
 *
 * This class is responsible for testing the CsvFileIterator class.
 */
#[CoversClass(FileIterator::class)]
#[CoversClass(CsvFileIterator::class)]
class CsvFileIteratorTest extends TestCase
{

    /**
     * Iterates over a CSV file and compares the values with the expected values.
     *
     * @param string $filename The path of the CSV file.
     * @param array $options
     * @return void
     */
    #[Test]
    #[TestDox('CsvFileIterator Test')]
    #[TestWith([__DIR__ . '/data/poi.csv'])]
    #[TestWith([__DIR__ . '/data/state.csv', ['separator' => '|']])]
    public function iterator(string $filename, array $options = []): void
    {
        $iterator = new CsvFileIterator($filename, $options);
        $expected = array_map(fn(string $line): array => str_getcsv($line, ...$options), file($filename));
        foreach($iterator as $row=>$value) {
            $this->assertEquals($expected[$row], $value);
        }
        $this->assertCount(iterator_count($iterator), $expected);
    }

}