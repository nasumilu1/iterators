<?php

namespace Nasumilu\Iterators\Tests;

use PDO;

final class DatabaseConnectionProvider
{

    private readonly array $connections;
    private static ?self $instances = null;

    private function __construct()
    {
        $this->connections = array_map(
            function (string $dsn): array {
                $connection = new PDO($_ENV[$dsn], $_ENV['DATABASE_USERNAME'], $_ENV['DATABASE_PASSWORD']);
                //$connection->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
                return [$connection];
            },
            ['mysql'=>'DATABASE_MYSQL_DSN', 'postgresql' => 'DATABASE_POSTGRESQL_DSN']
        );
        // PHP DateInterval best uses iso8601 style intervals
        $this->connections['postgresql'][0]->exec('SET intervalstyle = \'iso_8601\'');
    }

    private static function getInstance(): self {
        if (null === self::$instances) {
            self::$instances = new self();
        }
        return self::$instances;
    }

    public static function getConnection(string $vendor): PDO
    {
        foreach (self::connections() as $key=>$connection) {
            if ($key === $vendor) {
                return $connection[0];
            }
        }
        throw new \RuntimeException("PDO connection for $vendor not found!");
    }

    public static function connections(): array
    {
        return self::getInstance()->connections;
    }

}