<?php

namespace Nasumilu\Iterators\Tests;

use InvalidArgumentException;
use Nasumilu\Iterators\FileIterator;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\TestWith;
use PHPUnit\Framework\TestCase;

/**
 * FileIteratorTest class
 *
 * This class contains test cases for the FileIterator class.
 */
#[CoversClass(FileIterator::class)]
class FileIteratorTest extends TestCase
{

    /**
     * Iterates over the lines of a file.
     *
     * @param string $filename The path to the file.
     *
     * @return void
     */
    #[Test]
    #[TestDox('FileIterator')]
    #[TestWith([__DIR__ . '/data/poi.csv'])]
    public function iterator(string $filename): void
    {
        $iterator = new FileIterator($filename);
        $expected = file($filename);
        foreach($iterator as $line=>$value) {
            $this->assertEquals($expected[$line], $value);
        }
        $this->assertCount(iterator_count($iterator), $expected);
    }


    /**
     * Creates an invalid FileIterator object with an invalid filename.
     *
     * @return void
     * @throws InvalidArgumentException If the filename is invalid.
     */
    #[Test]
    #[TestDox('FileIterator with invalid filename!')]
    public function invalidIterator(): void
    {
        $this->expectException(InvalidArgumentException::class);
        $iterator = new FileIterator(__DIR__ . '/data/not-file.csv');
    }
}