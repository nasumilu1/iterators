<?php

namespace Nasumilu\Iterators\Tests;

use Nasumilu\Iterators\Collectors;
use Nasumilu\Iterators\FunctionalIterator;
use Nasumilu\Iterators\Gatherer;
use Nasumilu\Iterators\Gatherers;
use Nasumilu\Iterators\Iterators;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\TestWith;
use PHPUnit\Framework\TestCase;

#[CoversClass(Iterators::class)]
#[CoversClass(Gatherers::class)]
#[CoversClass(Gatherer::class)]
#[CoversClass(Collectors::class)]
#[CoversClass(FunctionalIterator::class)]
class GatherersTest extends TestCase
{

    #[Test]
    #[TestDox('Gatherers::windowFixed')]
    #[TestWith([2])]
    #[TestWith([3])]
    #[TestWith([4])]
    #[TestWith([5])]
    #[TestWith([6])]
    #[TestWith([10])]
    public function windowFixed(int $size): void {
        $data = [1, 2, 3, 4, 5, 6];
        $count = Iterators::from($data)->gather(Gatherers::windowFixed($size))
            ->collect(Collectors::count());
        $this->assertEquals(ceil(count($data) / $size), $count);
    }

    #[Test]
    #[TestDox('Gatherers::windowSliding')]
    #[TestWith([2])]
    #[TestWith([3])]
    #[TestWith([4])]
    #[TestWith([5])]
    #[TestWith([6])]
    #[TestWith([10])]
    public function windowSliding(int $size): void {
        $data = [1, 2, 3, 4, 5, 6, 7];
        $count = Iterators::from($data)->gather(Gatherers::windowSliding($size))
            ->collect(Collectors::count());
        $expectedCount = count($data);
        $expected = $size < $expectedCount ? $expectedCount - $size + 1 : 1;
        $this->assertEquals($expected, $count);
    }

    #[Test]
    #[TestDox('Gatherers::fold')]
    public function fold(): void {
        $data = [1, 2, 3, 4, 5, 6, 7];
        $first = Iterators::from($data)
            ->gather(Gatherers::fold(static fn (null|int $carry, int $value): string => ($carry ?? '') . $value))
            ->first();
        $this->assertEquals('1234567', $first);
    }

}