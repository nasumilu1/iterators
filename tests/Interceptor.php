<?php

namespace Nasumilu\Iterators\Tests;

class Interceptor extends \php_user_filter {
    public static $content = '';
    public function filter($in, $out, &$consumed, bool $closing): int
    {
        while($bucket = stream_bucket_make_writeable($in)) {
            self::$content .= $bucket->data;
            $consumed += $bucket->datalen;
            stream_bucket_append($out, $bucket);
        }
        return PSFS_FEED_ME;
    }
};