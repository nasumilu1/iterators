<?php
/*
 *  Copyright 2023 Michael Lucas <nasumilu@gmail.com>
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

namespace Nasumilu\Iterators\Tests;

use AppendIterator;
use ArrayIterator;
use ArrayObject;
use Exception;
use Generator;
use Iterator;
use Nasumilu\Iterators\CallbackIterator;
use Nasumilu\Iterators\Collector;
use Nasumilu\Iterators\Collectors;
use Nasumilu\Iterators\FunctionalIterator;
use Nasumilu\Iterators\Iterators;
use Nasumilu\Iterators\MapIterator;
use Nasumilu\Iterators\PeekIterator;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\Attributes\TestWith;
use PHPUnit\Framework\TestCase;
use stdClass;

/**
 * Class IteratorsTest
 *
 * This class contains test cases for the Iterators class.
 */
#[CoversClass(Iterators::class)]
#[CoversClass(PeekIterator::class)]
#[CoversClass(MapIterator::class)]
#[CoversClass(CallbackIterator::class)]
#[CoversClass(Collector::class)]
#[CoversClass(Collectors::class)]
class IteratorsTest extends TestCase
{

    /**
     * Test the Iterators::from method
     *
     * @param iterable $values
     * @return void
     * @throws Exception if the array cannot be converted to an iterator.
     */
    #[Test]
    #[TestDox('Iterators::from')]
    #[TestWith([[1, 2, 3, 4, 5]])]
    #[TestWith([new ArrayObject([1, 2, 3, 4, 5])])]
    #[TestWith([new ArrayIterator([1, 2, 3, 4, 5])])]
    public function from(iterable $values): void
    {
        $iterator = Iterators::from($values);
        $this->assertSame($iterator, Iterators::from($iterator));
        foreach ($iterator as $key => $value) {
            $this->assertIsNumeric($key);
            $this->assertIsNumeric($value);
            $this->assertContains($value, $values);
        }
        $this->assertCount(iterator_count($iterator), $values);
        $this->assertInstanceOf(Iterator::class, $iterator);
        $this->assertInstanceOf(Iterators::class, $iterator);
    }

    /**
     * Tests the Iterators::from with a Generator argument.
     *
     * @return void
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::from with Generator argument')]
    public function fromWithGeneratorArgument(): void
    {
        $length = 10;
        $generator = function () use ($length) {
            foreach (range(0, $length) as $value) {
                yield $value;
            }
        };

        $iterator = Iterators::from($generator());
        $this->assertCount($length + 1, iterator_to_array($iterator));
        $this->expectException(Exception::class);
        iterator_to_array($iterator);

    }

    /**
     * Tests the Iterators::from with an AppendIterator argument.
     *
     * @return void
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::from with AppendIterator argument')]
    public function fromWithIteratorArgument(): void
    {
        $values = [1, 2, 3, 4, 5];
        $values2 = [6, 7, 8, 9, 10];
        $expected = array_merge($values, $values2);
        $appendIterator = new AppendIterator();
        $appendIterator->append(new ArrayIterator($values));
        $appendIterator->append(new ArrayIterator($values2));
        $iterator = Iterators::from($appendIterator);
        foreach ($iterator as $key => $value) {
            $this->assertIsNumeric($key);
            $this->assertIsNumeric($value);
            $this->assertContains($value, $expected);
        }
        $this->assertCount(iterator_count($iterator), $expected);
        $this->assertInstanceOf(Iterator::class, $iterator);
        $this->assertInstanceOf(Iterators::class, $iterator);
    }

    /**
     * Tests the Iterator::all method.
     *
     * @param array $values
     * @return void
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterator::all')]
    #[TestWith([['one' => 1, 'two' => 2, 'three' => 3, 'four' => 4, 'five' => 5, 'six' => 6, 'seven' => 7]])]
    public function all(array $values): void
    {
        $predicate = fn(int $value): bool => $value >= 5;
        $expected = array_filter($values, $predicate);
        $iterator = Iterators::from($values)->filter($predicate);
        $this->assertCount(iterator_count($iterator), $expected);
        foreach ($iterator as $key => $value) {
            $this->assertIsNumeric($value);
            $this->assertIsString($key);
            $this->assertGreaterThanOrEqual(5, $value);
        }
    }

    /**
     * Tests the Iterators::values method.
     *
     * @param array $values
     * @return void
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::values')]
    #[TestWith([[1 => 'z', 2 => 'd', 3 => 'e', 4 => 'a', 5 => 'b']])]
    public function values(array $values): void
    {
        $iterator = Iterators::from($values);
        $this->assertCount(iterator_count($iterator), $values);
        $this->assertEquals($values, $iterator->values());
        $this->assertEquals('a', $iterator->values(true, false)[0]);
        $this->assertEquals('z',
            $iterator->values(fn(string $a, string $b): int => $b <=> $a, false)[0]);
    }


    /**
     * Tests the Iterators::every method.
     *
     * @param ArrayObject $values
     * @return void
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::every')]
    #[TestWith([new ArrayObject([true, true, true, true])])]
    public function every(ArrayObject $values): void
    {
        $iterator = Iterators::from($values);
        $predicate = fn(bool $value): bool => $value;
        $this->assertTrue($iterator->every($predicate));

        $values[] = false;
        $this->assertFalse($iterator->every($predicate));
    }

    /**
     * Test the Iterators::peek method.
     *
     * @param array $values
     * @return void
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::peek')]
    #[TestWith([[true, true, true, true]])]
    public function peek(array $values): void
    {
        $count = 0;
        $iterator = Iterators::from($values)
            ->peek(function (bool $value) use (&$count): void {
                $this->assertTrue($value);
                $count++;
            });
        iterator_to_array($iterator);
        $this->assertEquals(iterator_count($iterator), $count);

    }

    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::map')]
    public function map(): void
    {
        $squared = fn(int $value): int => pow($value, 2);
        $values = [1, 2, 3, 4, 5];
        $values2 = [6, 7, 8, 9, 10];
        $expected = array_map($squared, array_merge($values, $values2));
        $appendIterator = new AppendIterator();
        $appendIterator->append(new ArrayIterator($values));
        $appendIterator->append(new ArrayIterator($values2));
        $iterator = Iterators::from($appendIterator)
            ->map($squared);
        foreach ($iterator as $key => $value) {
            $this->assertContains($value, $expected);
            $this->assertIsNumeric($value);
            $this->assertIsNumeric($key);
        }
        $this->assertCount(iterator_count($iterator), $expected);
    }

    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::some')]
    #[TestWith([new ArrayObject([false, false, true, false])])]
    public function some(ArrayObject $values): void
    {
        $count = 0;
        $predicate = function (bool $value) use (&$count): bool {
            $count++;
            return $value;
        };
        $iterator = Iterators::from($values);
        $testValue = $iterator->some($predicate);
        $this->assertCount(iterator_count($iterator), $values);
        $this->assertEquals($count, count($values) - 1);
        $this->assertTrue($testValue);

        $values[2] = false;
        $this->assertFalse($iterator->some($predicate));

    }

    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::find')]
    public function find(): void
    {
        $predicate = fn(stdClass $obj): bool => $obj->name === 'test';
        $values = new ArrayObject([
            (object)['name' => 'one'],
            (object)['name' => 'two'],
            (object)['name' => 'test']
        ]);
        $iterator = Iterators::from($values);
        $value = $iterator->find($predicate);
        $this->assertSame($value, $values[2]);

        // default value
        $values[2]->name = 'not-test';
        $value = $iterator->find($predicate);
        $this->assertNull($value);
        $value = $iterator->find($predicate, false);
        $this->assertFalse($value);
    }

    /**
     * Tests the Iterators::first method.
     *
     * @param ArrayObject $values The array object containing the values.
     *
     * @return void
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::first')]
    #[TestWith([new ArrayObject([['name' => 'one'], ['name' => 'two'], ['name' => 'test']])])]
    public function first(ArrayObject $values): void
    {
        $iterator = Iterators::from($values);
        $value = $iterator->first();
        $this->assertEquals($value, $values[0]);

        // default value
        $values[2]['name'] = 'not-test';
        $predicate = fn(array $value): bool => $value['name'] === 'test';
        $value = $iterator->filter($predicate)->first();
        $this->assertNull($value);
        $value = $iterator->filter($predicate)->first(false);
        $this->assertFalse($value);

        $this->assertEquals(1, Iterators::from([1, 2, 3,])->first());

        $this->assertNull(Iterators::from([])->first());
    }

    /**
     * Applies a function to each element of the iterator.
     *
     * @param array $values
     * @return void
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::foreach')]
    #[TestWith([[1, 2, 3, 4, 'five' => 5, 6, 7]])]
    public function foreach(array $values): void
    {
        $count = 0;
        $iterator = Iterators::from($values);
        $fn = function () use (&$count): void {
            $count++;
        };
        $iterator->foreach($fn);
        $this->assertCount($count, $values);
    }

    /**
     * Test the Iterators::last method.
     *
     * @param array $values The array of values.
     * @return void
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::last')]
    #[TestWith([[1, 2, 3, 4, 5, 6]])]
    #[TestWith([[-23, -2, -44, 5, 0, 24, 3, 65, 1]])]
    public function last(array $values): void
    {
        $iterator = Iterators::from($values);
        $last = $iterator->last();
        $this->assertEquals($values[count($values) - 1], $last);
        $last = $iterator->filter(fn(int $value): bool => $value < 3)->last();
        $expected = array_filter($values, fn(int $value): bool => $value < 3);
        $this->assertEquals(end($expected), $last);
    }

    #[Test]
    #[TestDox('Interators::reduce')]
    public function reduce(): void
    {
        $values = [1, 2, 3, 4, 5];
        $expected = array_sum($values);
        $this->assertEquals(
            $expected,
            Iterators::from($values)->reduce(static fn(int $carry, int $value): int => $carry + $value, 0)
        );
    }


    /**
     * Test Iterators::concat
     *
     * @param array|ArrayObject ...$values
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators::contact')]
    #[TestWith([[1, 2, 3, 4, 5, 6], new ArrayObject([-23, -2, -44, 5, 0, 24, 3, 65, 1]), [6, 3, 4, 10]])]
    public function concat(array|ArrayObject ...$values): void
    {
        $values[] = Iterators::from([685, 25, 47]);
        $expected = array_merge(...array_map(
                static function (array|ArrayObject|FunctionalIterator $value): array {
                    if ($value instanceof ArrayObject) {
                        return $value->getArrayCopy();
                    } elseif ($value instanceof FunctionalIterator) {
                        return $value->values();
                    }
                    return $value;
                },
                $values)
        );
        $iterator = Iterators::concat(...$values);
        $iterator->foreach(function (int $value) use ($expected): void {
            $this->assertContains($value, $expected);
        });
        $this->assertCount(iterator_count($iterator), $expected);
    }

    /**
     * @throws Exception
     */
    #[Test]
    #[TestDox('Iterators collect test')]
    public function collect(): void
    {

        $fixture1 = [1, 2, 3, 4, 5, 6, 7];
        $fixture2 = [8, 9, 10, 11, 12, 13, 14, 15];
        $sum = Iterators::concat($fixture1, $fixture2)
            ->collect(Collectors::sum());
        $this->assertEquals(array_sum(array_merge($fixture1, $fixture2)), $sum);
    }

    #[Test]
    #[TestDox('Iterators::indexOf test')]
    public function indexOf(): void
    {
        $pages = [
            'image' => ['png', 'bmp', 'jpg'],
            'video' => 'mp4'
        ];
        $input = 'jpg';
        $predicate = fn(string $input): callable =>
            fn(array|string $value, string $key): string =>
                (is_array($value) && in_array($input, $value)) || $value === $input;

        $page = Iterators::from($pages)->search($predicate($input), 'error');
        $this->assertEquals('image', $page);
        $input = 'php';
        $page = Iterators::from($pages)->search($predicate($input), 'error');
        $this->assertEquals('error', $page);
    }

    #[Test]
    #[TestDox('Iterators::delay Test')]
    public function backpressure(): void
    {
        $iterations = 0;
        $generator = static function () use (&$iterations): Generator {
            do {
                $iterations--;
                yield Iterators::range(0, 100)
                    ->map(static fn(): int => rand(0, 100))
                    ->values();
            } while ($iterations > 0);
        };

        $sum = Iterators::from($generator)
            ->delay(0.1)
            ->map(static fn(array $value): float => array_sum($value))
            ->collect(Collectors::sum());

        // need better test here
        $this->assertGreaterThan(0, $sum);
        $this->assertEquals(-1, $iterations);
    }

    #[Test]
    public function testFlatMapWithCallback(): void
    {
        $data = [100, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 'twenty' => [21, 22, 23, 24, 25]];
        $fn = static fn(int $value): int => $value * 2;
        $expected = [];
        array_walk_recursive($data, function(int $value) use (&$expected): void { $expected[] = $value * 2;});
        $flat = Iterators::from($data)
            ->flatMap($fn)
            ->values(preserve_keys: false);

        $this->assertEquals($expected, $flat);
    }

    #[Test]
    public function testFlatMapWithoutCallbac(): void
    {
        $data = [100, [1, 2, 3, 4, 5, 6, 7, 8, 9, 10], 'twenty' => [21, 22, 23, 24, 25]];
        $expected = [];
        array_walk_recursive($data, function(int $value) use (&$expected): void { $expected[] = $value;});
        $flat = Iterators::from($data)
            ->flatMap()
            ->values(preserve_keys: false);

        $this->assertEquals($expected, $flat);
    }

}