<?php

namespace Nasumilu\Iterators\Tests;

use PDO;
use PHPUnit\Framework\TestCase;

class PdoTest extends TestCase
{

    private static ?PDO $dbh = null;

    public static function setUpBeforeClass(): void
    {
        self::$dbh = new PDO('sqlite::memory:');

    }



    public static function tearDownAfterClass(): void
    {
        self::$dbh = null;
    }

}