<?php

namespace Nasumilu\Iterators\Tests;

use Nasumilu\Iterators\CallbackIterator;
use Nasumilu\Iterators\Iterators;
use Nasumilu\Iterators\MapIterator;
use Nasumilu\Iterators\Stream;
use Nasumilu\Iterators\Stream\CsvStream;
use Nasumilu\Iterators\Stream\EchoStream;
use Nasumilu\Iterators\Stream\JsonStream;
use Nasumilu\Iterators\Streams;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\Test;
use PHPUnit\Framework\Attributes\TestDox;
use PHPUnit\Framework\TestCase;


#[CoversClass(Iterators::class)]
#[CoversClass(Stream::class)]
#[CoversClass(Streams::class)]
#[CoversClass(CallbackIterator::class)]
#[CoversClass(MapIterator::class)]
#[CoversClass(CsvStream::class)]
#[CoversClass(JsonStream::class)]
#[CoversClass(EchoStream::class)]
class StreamsTest extends TestCase
{


    #[Test]
    #[TestDox('Streams::json unit test')]
    public function json()
    {
        Interceptor::$content = '';
        $data = [
            [
                'name' => ['first' => 'John', 'last' => 'Doe'],
                'age' => 42
            ],
            [
                'name' => ['first' => 'Jane', 'last' => 'Doe'],
                'age' => 35
            ]
        ];
        $expected = json_encode($data);
        $iterator = Iterators::from($data);;
        stream_filter_register('interceptor', Interceptor::class);
        $out = fopen('php://stdout', 'w');
        stream_filter_append($out, 'interceptor');

        $iterator->stream(Streams::json(resource: $out));
        $this->assertJson(Interceptor::$content);
        $this->assertEquals($expected, Interceptor::$content);
        fclose($out);
    }

    #[Test]
    #[TestDox('Streams::csv unit test')]
    public function csv()
    {
        Interceptor::$content = '';
        $iterator = Iterators::from([
            [
                'name' => ['first' => 'John', 'last' => 'Doe'],
                'age' => 42
            ],
            [
                'name' => ['first' => 'Jane', 'last' => 'Doe'],
                'age' => 35
            ]
        ]);
        stream_filter_register('interceptor', Interceptor::class);
        $out = fopen('php://stdout', 'w');
        stream_filter_append($out, 'interceptor');

        $iterator->map(static fn(array $value): array => ['name' => implode(' ', $value['name']), 'age' => $value['age']])
            ->stream(Streams::csv(resource: $out));

        $this->assertStringContainsString('name,age', Interceptor::$content);
        $this->assertStringContainsString('"John Doe",42', Interceptor::$content);
        $this->assertStringContainsString('"Jane Doe",35', Interceptor::$content);
        fclose($out);
    }

    #[Test]
    #[TestDox('Streams::echo unit test')]
    public function echo()
    {
        Interceptor::$content = '';
        $data = [
            [
                'name' => ['first' => 'John', 'last' => 'Doe'],
                'age' => 42
            ],
            [
                'name' => ['first' => 'Jane', 'last' => 'Doe'],
                'age' => 35
            ]
        ];
        $expected = <<<EOL
0 => Array
(
    [name] => Array
        (
            [first] => John
            [last] => Doe
        )

    [age] => 42
)
1 => Array
(
    [name] => Array
        (
            [first] => Jane
            [last] => Doe
        )

    [age] => 35
)

EOL;
        $iterator = Iterators::from($data);;
        stream_filter_register('interceptor', Interceptor::class);
        $out = fopen('php://stdout', 'w');
        stream_filter_append($out, 'interceptor');

        $iterator->stream(Streams::echo(resource: $out));
        $this->assertEquals($expected, Interceptor::$content);
        fclose($out);
    }

}