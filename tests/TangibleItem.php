<?php

namespace Nasumilu\Iterators\Tests;

use Stringable;

final readonly class TangibleItem implements Stringable
{

    public function __construct(public string      $name,
                                public float       $value,
                                public float|null  $tax = null)
    {}

    public function taxable(): bool
    {
        return $this->tax != null;
    }

    public function total(): float
    {
        return $this->tax ? $this->value * (1 + $this->tax) : $this->value;
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public static function compare(TangibleItem $a, TangibleItem $b): int
    {
        return $a->total() <=> $b->total();
    }

}