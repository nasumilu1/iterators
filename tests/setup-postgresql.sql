create table if not exists person
(
    id   int primary key,
    name character varying(16) not null,
    dob  date        not null
);

create index person_dob_idx on person (dob);

insert into person (id, name, dob)
values (1, 'Marilyn Monroe', '1926-06-01'),
       (2, 'Abraham Lincoln', '1809-02-12'),
       (3, 'Edgar Allen Poe', '1849-10-07');

-- Contact
create table if not exists contact
(
    id    int primary key,
    type  character varying(16)  not null check ( type = 'phone' or type = 'email' or type = 'address' ),
    value character varying(128) not null
);

create index contact_type_idx on contact (type);

insert into contact (id, type, value)
values (1, 'phone', '123-123-4568'),
       (2, 'email', 'mmonro@actress.net'),
       (3, 'address', '245 N Beverly Dr, Beverly Hills, CA'),
       (4, 'phone', '852-741-8523'),
       (5, 'email', 'alincoln@ex-president.com'),
       (6, 'address', '1600 Pennsylvania Avenue NW, Washington, DC 20500'),
       (7, 'phone', '666-911-7894'),
       (8, 'email', 'epoe@poetry.io'),
       (9, 'address', '101 Tell-Tale Heart Ln, Midnight Dreary, AL 12345'),
       (10, 'phone', '555-376-5309');


-- Person Contact
create table if not exists person_contact
(
    person  int not null references person (id),
    contact int not null references contact (id)
);

insert into person_contact (person, contact)
values (1, 1),
       (1, 2),
       (1, 3),
       (1, 10),
       (2, 4),
       (2, 5),
       (2, 6),
       (3, 7),
       (3, 8),
       (3, 9);

-- Subscription
create table if not exists subscription
(
    id       int primary key,
    name     character varying(64) not null unique,
    duration interval not null default 'P1M'::interval
);

insert into subscription (id, name, duration)
    values
        (1, 'Basic', 'P1M'::interval),
        (2, 'Standard', 'P1M'::interval),
        (3, 'Premium', 'P3M'::interval);

-- Person Subscription
create table if not exists person_subscription
(
    id           int primary key,
    start_date        date not null,
    end_date          date not null,
    person       int  not null references person (id),
    fee          decimal(19, 4) not null, -- us dollar
    subscription int  not null references subscription (id)
);

insert into person_subscription (id, start_date, end_date, person, fee, subscription)
values
    (1, '2023-01-01', '2024-05-31', 9.99, 1, 2),
    (2, '2022-05-01', '2023-12-31', 11.99, 2, 3),
    (3, '2000-03-01', '2022-12-31', 19.99, 3, 1);

insert into person_contact (person, contact)
values (1, 1),
       (1, 2),
       (1, 3),
       (1, 10),
       (2, 4),
       (2, 5),
       (2, 6),
       (3, 7),
       (3, 8),
       (3, 9);
