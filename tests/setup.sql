create table president
(
    id    integer not null primary key,
    fname text    not null,
    mname text    null,
    lname text    not null,
    dob   text    not null
);

create table term
(
    id integer not null primary key,
    president     integer not null
        constraint person_membership_person_id_fk
            references president,
    start_date text not null,
    end_date text not null
);


insert into president (id, fname, mname, lname, dob)
values (1, 'George', null, 'Washington', '1732-02-22'),
       (2, 'John', null, 'Adams', '1735-10-30'),
       (3, 'Thomas', null, 'Jefferson', '1743-04-13'),
       (4, 'James', null, 'Madison', '1751-03-06'),
       (5, 'James', null, 'Monroe', '1758-04-28'),
       (6, 'John', 'Quincy', 'Adams', '1767-07-11'),
       (7, 'Andrew', null, 'Jackson', '1767-03-15');

insert into term (id, president, start_date, end_date)
values (1, 1, '1789-04-30', '');
